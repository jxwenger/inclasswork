/**********************************************
* File: googleProb.cpp
* Author: Jonathan Wenger
* Email: jwenger1@nd.edu
*
* Program to find the shortest word in a dictionary
* containing all of the characters from a given liscence plate
***********************************************/

#include<string>
#include<algorithm>
#include<vector>
#include <fstream>
#include <iostream>

/********************************************
  * Function Name  : findShortest
  * Pre-conditions : std::string
  * Post-conditions: std::string
  *
  * Method to find shortest word containing all letters
  * in a dictionary file
  ********************************************/
std::string findShortest(std::string plate){
  std::ifstream file("/users/jwenger/downloads/words.txt");
  std::vector<std::string> dictionary;
  std::string str;
  while (std::getline(file, str)){
    dictionary.push_back(str);
  }
  std::string letters="";
  for(int x=0;x<plate.length();x++){
    if(plate[x]>64){
      letters+=plate[x];
    }
  }
  std::string word="";
  for(int x=0;x<dictionary.size();x++){
    bool valid=true;
    std::string s = dictionary[x];
    for(int y=0;y<letters.length();y++){
      if (s.find(letters[y]) == std::string::npos){
        valid=false;
      }
    }
      if(valid&&(dictionary[x].length()<word.length()||word.length()==0)){
        word=dictionary[x];
      }

}
  return word;
}
/********************************************
  * Function Name  : main
  * Pre-conditions : int, char**
  * Post-conditions: int
  *
  * This is the driver method for the
  * find shortest program
  ********************************************/
int main(int argc, char** argv){
  std::cout<<findShortest(argv[1])<<std::endl;
  return 0;
}
