/**********************************************
* File: BTreeDatabase.cpp
* Author: Jonathan Wenger
* Email: jwenger1@nd.edu
*
* This is the driver function for your the Btree
* database
************************************************/

#include "BTree.h"
#include "BTreeNode.h"
#include <iostream>
struct Data{
  int key;
  int data;
  /********************************************
  * Function Name  : Data
  * Pre-conditions : void
  * Post-conditions: none
  *
  * This is the default constructor for the Data struct
  ********************************************/
  Data(){
    key=0;
    data=0;
  }
  /********************************************
  * Function Name  : Data
  * Pre-conditions : const Data &cp
  * Post-conditions: none
  *
  * This is copy constructor for the Data struct
  ********************************************/
  Data(const Data &cp){
    key=cp.key;
    data=cp.data;
  }
  /********************************************
  * Function Name  : Data
  * Pre-conditions : int k, int d
  * Post-conditions: none
  *
  * This is the constructor for the Data struct
  ********************************************/
  Data(int k, int d){
    key=k;
    data=d;
  }
  /********************************************
  * Function Name  : ~Data
  * Pre-conditions : void
  * Post-conditions: none
  *
  * This is the destructor for the Data struct
  ********************************************/
  ~Data(){}
  /********************************************
  * Function Name  : operator=
  * Pre-conditions : Data other
  * Post-conditions: Data&
  *
  * This is the overloaded assignment operator
  * for the Data struct.
  ********************************************/
  Data& operator=(Data other){
        std::swap(key, other.key);
        std::swap(data, other.data);
        return *this;
  }
  /********************************************
  * Function Name  : operator==
  * Pre-conditions : const Data other
  * Post-conditions: bool
  *
  * This is the overloaded == operator that
  * compares two Data instances based on their
  * key values.
  ********************************************/
  bool operator==(const Data other){
    return key==other.key;
  }
  /********************************************
  * Function Name  : operator>
  * Pre-conditions : const Data other
  * Post-conditions: bool
  *
  * This is the overloaded > operator that
  * compares two Data instances based on their
  * key values.
  ********************************************/
  bool operator>(const Data other){
    return key>other.key;
  }
  /********************************************
  * Function Name  : operator<
  * Pre-conditions : const Data other
  * Post-conditions: bool
  *
  * This is the overloaded < operator that
  * compares two Data instances based on their
  * key values.
  ********************************************/
  bool operator<(const Data other){
    return key<other.key;
  }
};


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* This is the main driver function for the program
********************************************/
int main(int argc, char** argv){
  BTree<Data> database(3);
  int i=0;
  srand(time(NULL));
  while(i<1000){
    Data elem(rand()%1000,rand()%1000);
    std::cout<<elem.key<<","<<elem.data<<std::endl;
    database.insert(elem);
    i++;
  }
  //database.traverse();
  for(int x=0; x<10;x++){
  Data val(rand()%1000,0);
  //  std::cout<<val.key<<std::endl;
    database.printFoundNodes(val);
  }
  return 0;
}
