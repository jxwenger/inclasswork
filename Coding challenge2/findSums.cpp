#include<iostream>
#include<map>

struct pair{
  pair(){
    num1=0;
    num2=0;
  }
  pair(int n1,int n2){
    num1=n1;
    num2=n2;
  }
  friend std::ostream& operator<<(std::ostream& outStream, const pair out){
      outStream<< out.num1 <<", " << out.num2;
      return outStream;
    }

  int num1;
  int num2;
};
pair *bruteForce(int arr[], int target, int size){
  pair nums[size/2];
  int i=0;
  for(int x=0;x<size;x++){
    for(int y=x+1;y<size;y++){
      if(arr[x]+arr[y]==target){
        nums[i]=pair(arr[x],arr[y]);
        i++;
      }
    }
  }

  return nums;
}
pair *hash(int arr[], int target, int size){
  std::map<int,int> hash_table;
  int i=0;
  pair nums[size/2];
  for(int x=0;x<size;x++){
      hash_table[target-arr[x]]=arr[x];
  }
  for(int x=0;x<size;x++){
    if(hash_table[x]==target-x){
      nums[i]=pair(x,target-x);
      i++;
    }
}
for(int x=0;x<5;x++)
  std::cout<<nums[x];
  return nums;
}

pair *sorted(int arr[], int target, int size){
  int front=0;
  int back=size-1;
  int i=0;
  pair nums[size/2];
  while(front<back){
    if(arr[front]+arr[back]>target){
      back--;
    }
    if(arr[front]+arr[back]<target){
      front++;
    }
    if(arr[front]+arr[back]==target){
      nums[i]=pair(arr[front],arr[back]);
      i++;
      if(arr[back-1]==arr[back])
        back--;
      else
        front++;
    }
    }
    return nums;
}
int main(){
  int arr[]={5,3,6,8,2,2,3,5,6};
  int sz=sizeof(arr)/sizeof(arr[0]);
  bruteForce(arr,10,sz);
  hash(arr,10,sz);
  sorted(arr,10,sz);

}
