/**********************************************
* File: AVLAuthorTest.cpp
* Author:
* Email:
*
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct Author{
  std::string firstName, lastName;
  Author(std::string f, std::string l){
    firstName=f;
    lastName=l;
  }
  bool operator==(const Author other){
    if(other.firstName!=firstName){
      return false;
    }
    if(other.lastName!=lastName){
      return false;
    }
    return true;
  }
  bool operator<(const Author other){
    if(other.lastName<lastName){
      return true;
    }
    else if(other.lastName==lastName){
    if(other.firstName>firstName){
      return true;
    }
  }
      return false;
    }
    friend std::ostream& operator<<(std::ostream& outStream, const Author printAuth){
      outStream<< printAuth.lastName <<", " << printAuth.firstName;
      return outStream;
    }
};// Author1, Author2, Author3, Author4, Author5;
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*
* This is the main driver program for the
* AVLTree function with strings
********************************************/
int main(int argc, char **argv)
{
  AVLTree<Author> avlAuthor;

  const Author A1("Aaron", "Aardvark");
  const Author A2("bad", "Aardvark");
  const Author A3("gregory", "Aardvark");
  avlAuthor.insert(A1);
  avlAuthor.insert(A2);
  avlAuthor.insert(A3);

  avlAuthor.printTree();
  avlAuthor.remove(A2);
  std::cout<<"----"<<std::endl;
  avlAuthor.printTree();


    return 0;
}
