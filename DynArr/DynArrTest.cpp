/*****************************************
 * Filename: DynArrTest.cpp
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 *
 * This file demonstrates the purpose of dynamic
 * memory allocation using templates
 *
 * array_test tests the copy constructor, assignment operator
 * and destructor of the templated class.
 *
 * Requires DynArray.h to run
 * **************************************/

#include <iostream> // for cout
#include <cstdlib> // for rand()
#include <ctime> // for time()
#include <vector>
#include "DynArray.h"

const int NITEMS = 1<<3;

 /******************************
 * Function Name: find
 * Preconditions: InputIterator first, InputIterator last, const T&
 * Postconditions: InputIterator
 *
 * Iterates from the first to the last element. If a value
 * is found, it returns the iterator of that element
 * Otherwise, it will return the last iterator
 * ****************************/
template<class InputIterator, class T>
InputIterator find (InputIterator first, InputIterator last, const T& val)
{
    while (first!=last) {
      if (*first==val)
        return first;
      ++first;
    }
    return last;
}

/********************************************
* Function Name  : insertAndPrint
* Pre-conditions : Array<int>& arrTest, std::vector<int>& vecTest
* Post-conditions: none
*
* inserts and prints random values into the array and vector
********************************************/
void insertAndPrint(Array<int>& arrTest, std::vector<int>& vecTest){

	std::cout << "The code const int NITEMS = 1<<3; makes the int: " << NITEMS << std::endl;

	srand(time(NULL));
	std::cout << "arrTest[i] arrTest.size() arrTest.size() / vecTest.at(i) vecTest.size() vecTest.capacity()" << std::endl;
    for (int i = 0; i < NITEMS + 1; i++) {
		int insertVal = rand() % 1000;
    	arrTest.push_back(insertVal);
		vecTest.push_back(insertVal);
		std::cout << "Array: " << arrTest[i] << " " << arrTest.size() << " " << arrTest.getCapacity() << " / ";
		std::cout << "Vector: " << vecTest.at(i) << " " << vecTest.size() << " " << vecTest.capacity() * 2 << std::endl;
    }

	std::cout << "The capacity of the array is " << arrTest.getCapacity() << std::endl;
	std::cout << "The capacity of the vector is " << vecTest.capacity() * 2 << std::endl;

}

/********************************************
* Function Name  : ArrayIterate
* Pre-conditions : Array<int>& arrTest
* Post-conditions: none
*
* Iterate Through the Dynamic Array
********************************************/
void ArrayIterate(Array<int>& arrTest){

	std::cout << "This is the result of the array iteration" << std::endl;
	for (int i = 0; i < arrTest.size(); i++) {
        int* iter;
    	if ((iter = find(arrTest.begin(), arrTest.end(), arrTest[i])) != arrTest.end()) {

    	    std::cout << i << ": " << *iter << " and " << arrTest[i] << std::endl;

    	}
    }

}

/********************************************
* Function Name  : VectorIterate
* Pre-conditions : std::vector<int>& vecTest
* Post-conditions: none
*
* Iterate through the vector
********************************************/
void VectorIterate(std::vector<int>& vecTest){

	std::cout << "This is the result of the vector iteration" << std::endl;
	for (int i = 0; i < vecTest.size(); i++) {
		std::cout << i << ": " << vecTest.at(i) << std::endl;
	}

}

/************************************
 * Function name: main
 * Preconditions: int, char **
 * Postconditions: int
 *
 * This is the main driver function
 * *********************************/
int main(int argc, char**argv)
{
	Array<int> arrTest;
	std::vector<int> vecTest;

	insertAndPrint(arrTest, vecTest);

	// Iterate through Array
	//ArrayIterate(arrTest);

	// Iterate through the Vector
	//VectorIterate(vecTest);

	return 0;
}
